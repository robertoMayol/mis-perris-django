from django import forms
from .models import Postulante
from .models import Rescatado

class PostulanteForm(forms.ModelForm):
    
    class Meta:

        model = Postulante
        fields = '__all__' 
        
        años = ('1980', '1981', '1982', '1983', '1984', '1985', '1986', '1987',
       '1988', '1989', '1990', '1991', '1992', '1993', '1994', '1995',
       '1996', '1997', '1998', '1999', '2000', '2001', '2002', '2003',
       '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011',
       '2012', '2013', '2014', '2015')

        labels = {
            'nombre': 'Nombre',
            'correo': 'Correo',
            'rut': 'Rut',
            'telefono': 'Telefono',
            'fechaNacimiento': 'Fecha de Nacimiento',
            'region': 'Region',
            'comuna': 'Comuna',
            'vivienda': 'Vivienda',
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'correo': forms.TextInput(attrs={'class':'form-control'}),
            'rut': forms.TextInput(attrs={'class':'form-control'}),
            'telefono': forms.NumberInput(attrs={'class':'form-control'}),
            'fechaNacimiento': forms.SelectDateWidget(years = años),
            'region': forms.Select(attrs={'id':'regiones'}),
            'comuna': forms.Select(attrs={'id':'comunas'}),                        
            'vivienda': forms.TextInput(attrs={'class':'form-control'}),
            
        }

class RescatadoForm(forms.ModelForm):
    
    class Meta:

        model = Rescatado
        fields = [
            'foto',
            'nombre',
            'raza',
            'descripcion',
            'estado',
            
        ]
        labels = {
            'foto': 'Foto',
            'nombre': 'Nombre',
            'raza': 'Raza',
            'descripcion': 'Descripcion',
            'estado': 'Estado',
        }
        widgets = {
            'foto' : forms.FileInput(attrs={'class':'form-control'}),
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'raza': forms.TextInput(attrs={'class':'form-control'}),
            'descripcion': forms.TextInput(attrs={'class':'form-control'}),
            'estado': forms.TextInput(attrs={'class':'form-control'}),
        }
    
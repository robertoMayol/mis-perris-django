from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext as _


# Create your models here.

class Postulante(models.Model):
    nombre = models.CharField(max_length=40)
    correo = models.CharField(max_length=30)
    rut = models.CharField(max_length=10)
    telefono = models.IntegerField()
    fechaNacimiento = models.DateField()
    region = models.CharField(max_length=20)
    comuna = models.CharField(max_length=20)
    vivienda = models.CharField(max_length=20)
    
    def __str__(self):
        return '{}'.format(self.nombre)

    class Meta:
        permissions = (
            ('postulante',_('Postulante')),
    )


class Rescatado(models.Model):
    
    foto = models.FileField(upload_to="media/")
    nombre = models.CharField(max_length=40)
    raza = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=100)
    estado = models.CharField(max_length=10)    

    def __str__(self):
        return '{}'.format(self.nombre)

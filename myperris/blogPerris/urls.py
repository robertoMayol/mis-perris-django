from django.conf.urls import include, url
from . import views
from .views import RescatadoList


urlpatterns = [
    url(r'^$', views.post_list, name='perris'),
    url(r'^registro_postulante$', views.postulante_view,),
    url(r'^registro_rescatado$', views.rescatado_view,),
    url(r'^listar_rescatados$', RescatadoList.as_view(),),
]

from django.shortcuts import render, redirect
from django.views.generic import ListView
from .forms import PostulanteForm
from .forms import RescatadoForm   
from .models import Postulante
from .models import Rescatado
from django.contrib.auth.decorators import login_required, permission_required


# Create your views here.

def post_list(request): 
    user = request.user
    posts = Postulante.objects.filter()
    return render(request, 'blogPerris/MisPerris.html', {'posts': posts})


def post_list(request): 
    user = request.user
    posts = Rescatado.objects.filter()
    return render(request, 'blogPerris/MisPerris.html', {'rescatados': posts})

    

def postulante_view(request): 
    if request.method == 'POST':
        form = PostulanteForm(request.POST)
        if form.is_valid():
            form.save()
        return render(request,'blogPerris/MisPerris.html',)
        
    else:
        form = PostulanteForm()

    return render(request, 'blogPerris/formPerris.html', {'form':form} )

def rescatado_view(request):
    if request.method == 'POST':
        form = RescatadoForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            form.save()
        return render(request,'blogPerris/rescatado_list.html',)
        
    else:
        form = RescatadoForm()

    return render(request, 'blogPerris/perrisList.html', {'form':form} )

   

class RescatadoList(ListView):
    model = Rescatado
    template_name = 'rescatado_list.html'